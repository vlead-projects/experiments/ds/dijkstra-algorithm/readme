#+TITLE: STRUCTURE FOR DIJKSTRAS ALGORITHM
#+AUTHOR: VLEAD
#+DATE: [2018-12-6 Thu]
#+SETUPFILE: org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: \n:t

* Introduction
  This document captures the structure for the selection
  sort experiment.

* Experiment Structure
  Experiment consists of a preamble with a set of artefacts,
  where the overview of the overall experiment is
  defined. Then it is followed by a learning units. Each
  learning unit consists of a mandatory preamble with a set
  of artefacts, which describes it. Then each learning unit
  is followed by a set of tasks and quizzes. Each task is
  followed by a set of artefacts.

** =EXP : Dijkstras Algorithm=
*** LEARNING OBJECTIVES OF THIS EXPERIMENT
    + Given an unsorted array of numbers, generate a sorted
      array of numbers by applying Selection Sort.
    + Demonstrate knowledge of time complexity of Selection
      Sort by counting the number of operations involved in
      each iteration.
    + Compare Selection Sort with other sorting algorithms
      and realise Selectionsort as a stable comparison
      sorting algorithm.

*** DESCRIPTION OF VIDEO
    + Welcome the user to experiment
    + Overview of the experiment & aims
    + Flow of the experiment and tips (recap before taking
      pre-test, go through LU videos, do the practice
      exercises properly etc)

*** Preamble
    + *Text Artefact*  : Estimated time 
    + *Video Artefact* : Introduction to module and tips for user.
    + *Text Artefact*  : Prerequisites
    + *Text Artefact*  : Learning objectives
    + *Text Artefact*  : Weightage of modules

** =LU 1: Pre Test=
*** LEARNING OBJECTIVES OF THIS LEARNING UNIT
    + Test knowledge on sorting, time complexity and space
      complexity.
    + Test knowledge of operations on simple array.

*** Preamble
    + *Text Artefact* : Estimated Time
    + *Text Artefact* : Instructions for Pre-Test

*** Task : Recap
    + *Text Artefact*  : What is Sorting?
    + *Image Artefact* : Unsorted and Sorted arrays
    + *Text Artefact*  : Time and Space Complexity

*** Quiz : Pre-test Quiz
    It has simple multiple choice questions quiz to let the
    user self test his/her understanding on "Recap" task.

** =LU 2: Selection Sort=
*** LEARNING OBJECTIVES OF THIS LEARNING UNIT
    + Gain the intuition behind Selection Sort
    + Watch and visualise the selection sort algorithm.

*** Preamble
    + *Text Artefact*  : Estimated Time
    + *Video Artefact* : Selection Sort Concept
    + *Text Artefact*  : Learning objectives 

*** Task : Concept and Strategy (Remembering, Understanding)
    + *Text Artefact*  : How can we sort an array?
    + *Image Artefact* : Intuition Behind the Algorithm
    + *Text Artefact*  : Selection Sort Concept  
    + *Image Artefact* : Selection Sort Algorithm
    + *Text Artefact*  : Consolidated Algorithm for Selection Sort 

*** Task : Demo (Visualising, Understanding)
    + *Demo artefact* : will contain play/pause button, user
      can generate random set of numbers and watch them get
      sorted using selection sort. User can adjust speed
      using a slider and can click on "next step" manually
      to see the steps at his/her own pace. A box will show
      the count for comparisons, swaps and iterations.

*** Task : Practice (Applying)
    + *Practice artefact* : Similar to demo artefact. Here,
      user has the option to "update the minimum" , or move
      on to the "next" elements. User receieves a step by
      step feedback. If his/her step is wrong, a box will
      show feedback and give them the chance to try
      again. Wrong move will not be made in this case.

*** Task : Exercise (Applying)
    + *Exercise artefact* : Solve questions related to
      selection sort using artefact. Feedback will only be
      given at the end, when the user submits their answer.

*** Quiz : Quiz (Applying)
    + Questions to test basic understanding 

** =LU 3: Analysis=
*** LEARNING OBJECTIVES OF THIS LEARNING UNIT
    + Time and Space Complexity : We will learn about the
      running time of one iteration, and then extend it to N
      iterations to complete the sorting process.
    + Stable Sort : We will learn about stability of sorting
      algorithms. Then we will see how Selectione sort is a
      stable sort.
    + Comparison with other algorithms : We will compare
      Selection sort with other sorting algorithms.

*** Preamble
    + *Text Artefact*  : Estimated Time
    + *Video Artefact* : N^2 complexity
    + *Text Artefact*  : Learning objectives

*** Task : Time and space complexity (Remembering and Understanding)
    + *Text Artefact* : Running Time of Selection Sort
    + *Demo artefact* : artefact that count number of
       comparisons randomly ordered arrays.
    + *Text Artefact* : Space Complexity of Selection Sort

*** Task : Stability of Selection sort (Remembering and Understanding)
    + *Text Artefact*  : What is stable sort 
    + *Image Artefact* : stable sort and unstable sort
    + *Text Artefact*  : Selection sort as a stable sort
    + *Image Artefact* : Example

*** Task : Comparison with other sorting algorithms (Remembering and Understanding)
    + *Image Artefact* : Time complexity graph
    + *Text Artefact*  : Comparison table

*** Task : Small Quiz (Applying)
    + Questions to test basic understanding 

** =LU 4: Quiz=
*** LEARNING OBJECTIVES OF THIS LEARNING UNIT
    + Test user knowledge by application based questions.
    + Give single and multi correct questions.

*** Preamble
    + *Text Artefact* : Estimated Time
    + *Text Artefact* : Instructions for Quiz

*** Task : Post-Test Quiz (Applying, Testing)
    + Questions to test his/her understanding on overall
      experiment

** =LU 5: Further Readings=
*** Preamble
    + *Text Artefact* : Useful links for Coding
      implementation, Visualizations and online mcq quizzes

